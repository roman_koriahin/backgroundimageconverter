﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace BackgroundImageConverter
{
    partial class Watcher
    {
        public string dirPath;

        FileActions fileActions = new FileActions();

        FileSystemWatcher watcher;

        bool enabled = true;

        public Watcher(string dirPath)
        {
            try
            {
                this.dirPath = dirPath;

                watcher = new FileSystemWatcher(dirPath, "*.jpg");

                watcher.Deleted += Watcher_Deleted;
                watcher.Created += Watcher_Created;
                watcher.Changed += Watcher_Changed;
                watcher.Renamed += Watcher_Renamed;
            }
            catch
            {
                MessageBox.Show("Ошибка!\nВозможно " + dirPath + "является файлом, а не папкой!", "BackgroundImageConverter");
            }
        }

        public void Start()
        {
            watcher.EnableRaisingEvents = true;

            while (enabled)
            {
                Thread.Sleep(1000);

                GC.Collect();
            }
        }

        public void Stop()
        {
            watcher.EnableRaisingEvents = false;
            enabled = false;
        }

        private void Watcher_Renamed(object sender, RenamedEventArgs e)
        {
            string newFilePath = e.FullPath;
            string oldFilePath = e.OldFullPath;

            if (File.Exists(newFilePath))
            {
                fileActions.RenameFile(oldFilePath, newFilePath);
            }
        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            string filePath = e.FullPath;

            if (File.Exists(filePath))
            {
                fileActions.ResizeAndSaveImg(filePath);
            }
        }

        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            string filePath = e.FullPath;

            if (File.Exists(filePath))
            {
                fileActions.ResizeAndSaveImg(filePath);
            }
        }

        private void Watcher_Deleted(object sender, FileSystemEventArgs e)
        {
            string filePath = e.FullPath;

            fileActions.DeleteFile(filePath);
        }
    }
}
