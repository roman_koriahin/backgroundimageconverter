﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace BackgroundImageConverter
{
    static class Program
    {
        static FileActions fileActions = new FileActions();

        static MainWatcher mainWatcher;
        static System.Timers.Timer timer;

        static void Main()
        {
            Thread checkerThread = new Thread(new ThreadStart(InitializeTimer));
            checkerThread.Start();

            mainWatcher = new MainWatcher();
            Thread watcherThread = new Thread(new ThreadStart(mainWatcher.Start));
            watcherThread.Start();
        }

        static void InitializeTimer()
        {
            Timer_Elapsed(null, null);

            timer = new System.Timers.Timer();
            timer.Enabled = true;
            timer.Interval = 86400000;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        static private void Timer_Elapsed(object sender, EventArgs e)
        {
            try
            {
                string rootPath = @"D:\inetpub\wwwroot\HomeCinema\Images\Photos";

                DirectoryInfo di = new DirectoryInfo(rootPath);

                DirectoryInfo[] directories = di.GetDirectories();

                foreach (var directory in directories)
                {
                    string previewFolder = directory.FullName + @"\Preview";

                    FileInfo[] files = directory.GetFiles("*.jpg");

                    foreach (var file in files)
                    {
                        if (!File.Exists(previewFolder + @"\" + file.Name))
                        {
                            fileActions.ResizeAndSaveImg(file.FullName);
                        }
                    }

                    DirectoryInfo dii = new DirectoryInfo(previewFolder);

                    FileInfo[] filesi = dii.GetFiles();

                    List<string> filesToDelete = new List<string>();

                    foreach (var file in filesi)
                    {
                        if (!File.Exists(directory.FullName + @"\" + file.Name))
                        {
                            filesToDelete.Add(file.FullName);
                        }
                    }

                    foreach (var file in filesToDelete)
                    {
                        File.Delete(file);
                    }

                    filesToDelete.Clear();

                    files = null;
                    dii = null;
                    filesi = null;
                }

                directories = null;
                di = null;
            }
            catch
            {

            }

            GC.Collect();
        }
    }
}

//some note
