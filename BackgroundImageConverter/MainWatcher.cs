﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace BackgroundImageConverter
{
    class MainWatcher
    {
        FileSystemWatcher watcher;

        Watcher dirWatcher;

        List<Watcher> watcherList = new List<Watcher>();

        string rootPath = @"D:\inetpub\wwwroot\HomeCinema\Images\Photos";

        bool enabled = true;

        public MainWatcher()
        {
            try
            {
                watcher = new FileSystemWatcher(rootPath);

                watcher.Deleted += Watcher_Deleted;
                watcher.Created += Watcher_Created;
                watcher.Changed += Watcher_Changed;
                watcher.Renamed += Watcher_Renamed;
            }
            catch
            {
                MessageBox.Show("Ошибка!\nВозможно " + rootPath + "является файлом, а не папкой!", "BackgroundImageConverter");
            }
        }

        public void Start()
        {
            watcher.EnableRaisingEvents = true;

            StartWatchers();

            while (enabled)
            {
                Thread.Sleep(1000);

                GC.Collect();
            }
        }

        private void StartWatchers()
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(rootPath);
                DirectoryInfo[] directories = di.GetDirectories();

                foreach (var directory in directories)
                {
                    StartWatcher(directory.FullName);
                }

                dirWatcher = null;
                di = null;
                directories = null;
            }
            catch
            {
                MessageBox.Show("Ошибка!\nНе возможно получить список дерикторий!", "BackgroundImageConverter");
            }
        }

        private void StartWatcher(string dirPath)
        {
            try
            {
                if (Directory.Exists(dirPath))
                {
                    dirWatcher = new Watcher(dirPath);
                    Thread watcherThread = new Thread(new ThreadStart(dirWatcher.Start));
                    watcherThread.Start();

                    watcherList.Add(dirWatcher);
                }
            }
            catch
            {
                MessageBox.Show("Ошибка!\nНе возможно запустить процесс слежения за дерикторией\n" + dirPath, "BackgroundImageConverter");
            }
        }

        private void StopWatcher(string dirPath)
        {
            try
            {
                foreach (var item in watcherList)
                {
                    if (item.dirPath == dirPath)
                    {
                        item.Stop();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Ошибка!\nНе возможно остановить процесс слежения за дерикторией!", "BackgroundImageConverter");
            }
        }

        private bool Contains(List<Watcher> lst, string dirPath)
        {
            foreach (var item in lst)
            {
                if (item.dirPath == dirPath)
                {
                    return true;
                }
            }

            return false;
        }

        private void Watcher_Renamed(object sender, RenamedEventArgs e)
        {
            StopWatcher(e.OldFullPath);

            StartWatcher(e.FullPath);
        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            
        }

        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            StartWatcher(e.FullPath);
        }

        private void Watcher_Deleted(object sender, FileSystemEventArgs e)
        {
            StopWatcher(e.FullPath);
        }
    }
}
