﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace BackgroundImageConverter
{
    class FileActions
    {
        public void ResizeAndSaveImg(string filePath)
        {
            try
            {
                FileStream photo = File.Open(filePath, FileMode.Open, FileAccess.Read);
                BitmapDecoder decoder = BitmapDecoder.Create(photo, BitmapCreateOptions.IgnoreColorProfile, BitmapCacheOption.Default);
                BitmapMetadata metadata = (BitmapMetadata)decoder.Frames[0].Metadata.Clone();

                Bitmap bitmap = new Bitmap(photo);

                if (ReadImageProperty(metadata, "/app1/ifd/{uint=274}") == "6")
                {
                    bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                }
                else if (ReadImageProperty(metadata, "/app1/ifd/{uint=274}") == "8")
                {
                    bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                }
                else if (ReadImageProperty(metadata, "/app1/ifd/{uint=274}") == "3")
                {
                    bitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
                }

                string folderPath = GetFolderFromFilePath(filePath);
                string fileName = GetFileNameFromFilePath(filePath);

                int nWidth = 300;
                double tmp = Convert.ToDouble(bitmap.Width) / Convert.ToDouble(bitmap.Height);
                int nHeight = Convert.ToInt32(nWidth / tmp);

                Image result = new Bitmap(nWidth, nHeight);

                using (Graphics g = Graphics.FromImage((Image)result))
                {
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.DrawImage(bitmap, 0, 0, nWidth, nHeight);
                    g.Dispose();
                }

                bitmap.Dispose();

                string saveFolderPath = folderPath + @"\Preview\";

                if (!Directory.Exists(saveFolderPath))
                {
                    Directory.CreateDirectory(saveFolderPath);
                }

                string savePath = folderPath + @"\Preview\" + fileName;

                result.Save(savePath);

                result.Dispose();

                photo.Close();
            }
            catch
            {
                //MessageBox.Show("Ошибка!\nНе возможно конвертировать или сохранить изображение!\n" + filePath, "BackgroundImageConverter");
            }
        }

        private string ReadImageProperty(BitmapMetadata metadata, string queryString, bool _flag = false)
        {
            string result = "";

            try
            {
                object obj = metadata.GetQuery(queryString);

                result = Convert.ToUInt16(obj).ToString();

            }
            catch
            {
                result = null;

                //MessageBox.Show("Ошибка!\nНе возможно прочитать или преобразовать тег метаданных изображения!", "BackgroundImageConverter");
            }

            return result;
        }

        public string GetFolderFromFilePath(string filePath)
        {
            return new string(filePath.ToCharArray(0, filePath.LastIndexOf('\\')));
        }

        public string GetFileNameFromFilePath(string filePath)
        {
            int idx = filePath.LastIndexOf('\\');

            return new string(filePath.ToCharArray(idx + 1, filePath.Length - idx - 1));
        }

        public void RenameFile(string oldFilePath, string newFilePath)
        {
            try
            {
                string folderPath = GetFolderFromFilePath(oldFilePath);
                string oldFileName = GetFileNameFromFilePath(oldFilePath);
                string newFileName = GetFileNameFromFilePath(newFilePath);

                string folder = folderPath + @"\Preview\";


                File.Move(folder + oldFileName, folder + newFileName);
            }
            catch
            {
                //MessageBox.Show("Ошибка!\nНе возможно переименовать файл\n" + oldFilePath + "\nв\n" + newFilePath, "BackgroundImageConverter");
            }
        }

        public void DeleteFile(string filePath)
        {
            try
            {
                string folderPath = GetFolderFromFilePath(filePath);
                string fileName = GetFileNameFromFilePath(filePath);

                string deleteFilePath = folderPath + @"\Preview\" + fileName;

                File.Delete(deleteFilePath);
            }
            catch
            {
                MessageBox.Show("Ошибка!\nНе возможно удалить файл" + filePath, "BackgroundImageConverter");
            }
        }
    }
}
