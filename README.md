# BackgroundImageConverter

Программа предназначена для работы с моим сайтом-фотоальбомом\кинотеатром
(https://gitlab.com/roman_koriahin/homecinema.git)

Если программа запущена, то при добавлении/удалении/изменении фотографий в
"Images\Photos\<Название_фотоальбома>", программа автоматически делает уменьшенные
превьюшки этих фотографий и кладёт их в "Images\Photos\<Название_фотоальбома>\Preview"
